<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public function billLading()
    {
        return $this->hasMany(BillLading::class,'shop_id','id');
    }
}
