<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

class VTPLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vtp:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'VTP location list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $cities = Curl::to('https://partner.viettelpost.vn/v2/categories/listProvince')
//            ->asJson()
//            ->get();
//        $cities = $cities->data;
//        foreach ($cities as $city){
//            DB::table('vtp_cities')->insert([
//                'PROVINCE_ID' => $city->PROVINCE_ID,
//                'PROVINCE_CODE' => $city->PROVINCE_CODE,
//                'PROVINCE_NAME' => $city->PROVINCE_NAME
//            ]);
//        }

//        $cities = DB::table('vtp_cities')->get();
//        foreach ($cities as $city) {
//            $districts = Curl::to('https://partner.viettelpost.vn/v2/categories/listDistrict?provinceId=' . $city->PROVINCE_ID)
//                ->asJson()
//                ->get();
//            $districts = $districts->data;
//            foreach ($districts as $district) {
//                DB::table('vtp_districts')->insert(
//                    [
//                        "DISTRICT_ID" => $district->DISTRICT_ID,
//                        "DISTRICT_VALUE" => $district->DISTRICT_VALUE,
//                        "DISTRICT_NAME" => $district->DISTRICT_NAME,
//                        "PROVINCE_ID" => $district->PROVINCE_ID
//                    ]
//                );
//            }
//        }

        $districts = DB::table('vtp_districts')->get();
        foreach ($districts as $district) {
            $this->info($district->DISTRICT_ID);
            $wards = Curl::to('https://partner.viettelpost.vn/v2/categories/listWards?districtId=' . $district->DISTRICT_ID)
                ->asJson()
                ->get();
            $wards = $wards->data;
            if (is_object($wards) || is_array($wards)){
                foreach ($wards as $ward) {
                    DB::table('vtp_wards')->insert(
                        [
                            "WARDS_ID" => $ward->WARDS_ID,
                            "WARDS_NAME" => $ward->WARDS_NAME,
                            "DISTRICT_ID" => $ward->DISTRICT_ID
                        ]
                    );
                }
            }

        }
    }
}
