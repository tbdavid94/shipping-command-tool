<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateShop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:shop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake shop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $number = $this->ask('So shop muon tao', 10);
        $shop = collect();
        $retailerId = 300108;
        for ($i = 1; $i <= $number; $i++) {
            $payload['email'] = Str::random(5) . '@occ.dev';
            $payload['firstname'] = Str::random(5);
            $payload['lastname'] = Str::random(5);
            $payload['phone'] = Str::random(10);
            $payload['address'] = Str::random(15);
            $payload['retailer_id'] = $retailerId;
            $payload['code'] = 'testship'.$i ; //'FAKE' . Str::random(7) . date('s') . rand(10, 100);
            $payload['type'] = 3;//fake
            $payload['city_id'] = 2;
            $payload['district_id'] = 35;
            $payload['ward_id'] = 682;

            $shop->push($payload);
            $this->info('Fake thanh cong shop ' . $payload['code']);
            $retailerId++;
            if ($shop->count() == 1000) {
                $this->info('Insert ' . $shop->count() . ' shop ...');
                DB::table('shops')->insert($shop->unique('code')->values()->toArray());
                $this->info('Insert success');
                $shop = collect();

            }
        }
        $this->info('Insert ' . $shop->count() . ' shop ...');
        DB::table('shops')->insert($shop->unique('code')->values()->toArray());
        $this->info('Insert success');
        $this->info('Total shop fake '.DB::table('shops')->where('type',3)->count());
        $this->info('Done !');
    }
}
