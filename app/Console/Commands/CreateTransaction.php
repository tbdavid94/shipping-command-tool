<?php

namespace App\Console\Commands;

use App\Shop;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:transaction {shop_code?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shopCode = $this->argument('shop_code');
        if (isset($shopCode) && !empty($shopCode)) {
            $shop = Shop::query()->where('code', $shopCode)->first();
        } else {
            $shop = Shop::query()->inRandomOrder()->first();
        }

        $confirm = $this->ask('Xoa cac transaction hien tai cua shop nay khong ?', 1);
        if ($confirm == 1){
            DB::table('transactions')->where('shop_id',$shop->id)->delete();
        }

        $number = $this->ask('So transaction can tao', 10);
        $transactionData = collect();
        for ($i = 0; $i < $number; $i++) {
            $type = 1;//phieu doi soat
            $notIn = DB::table('transactions')->where('shop_id',$shop->id)->pluck('refer_id')->toArray();
            $refer = DB::table('cross_checks')->where('type',3)->where('shop_id',$shop->id)->whereNotIn('id',$notIn)->inRandomOrder()->first()->id;
            if ($i % 2 == 0){
                $type = 2;//phieu chi
                $refer = DB::table('cash_book_news')->inRandomOrder()->first()->id;
            }
            $item['amount'] = rand(1,1000);
            $item['balance'] = rand(1,1000);
            $item['balance_before'] = rand(1,1000);
            $item['created_at'] = now()->format('Y-m-d H:i:s');
            $item['updated_at'] = now()->format('Y-m-d H:i:s');
            $item['note'] = Str::random(10);
            $item['type'] = $type;
            $item['refer_id'] = $refer;
            $item['shop_id'] = $shop->id;
            $item['user_id'] = User::query()->inRandomOrder()->first()->id;
            $item['wallet'] = rand(1,1000);
            $item['wallet_before'] = rand(1,1000);
            $transactionData->push($item);
        }

        DB::table('transactions')->insert($transactionData->toArray());
    }
}
