<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TimetableSupportShipping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timetable:support';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Timetable support shipping schedule';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \Google_Client();
        $client->setApplicationName('My PHP App');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');

        $jsonAuth = storage_path('auth.json');
        $jsonAuth = json_decode(file_get_contents($jsonAuth), true);
        $client->setAuthConfig($jsonAuth);

        $service = new \Google_Service_Sheets($client);
        $spreadsheetId = env('GG_SHEET_ID');
        $range = 'Lịch trực';
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();

        $today = $values[0];
        $employee = $today ? $today[6] : '';


    }
}
