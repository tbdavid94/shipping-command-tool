<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateCrossCheckClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:cross-check-client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake cross check client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = DB::table('users')->inRandomOrder()->first();
        $client = DB::table('clients')->where('status',1)->inRandomOrder()->first();
        $this->info('1: HVC COD, 2: HVC PVC, 3: DSGH, 4: HVC CPS, 5 DS boi hoan GH');
        $type = $this->ask('Loai doi soat',1);
        $this->info('0: Luu tam, 1: Da xa nhan, 2: Da huy, 3: Da thanh toan, 4: Da xac nhan va gui mail, 5: Dang xu ly doi soat, 6: Loi doi soat');
        $status = $this->ask('Trang thai',0);
        $startPeriod = $this->ask('Ky doi soat bat dau', Carbon::now()->firstOfMonth()->format('Y-m-d'));
        $endPeriod = $this->ask('Ky doi soat ket thuc', Carbon::now()->lastOfMonth()->format('Y-m-d'));

        $periodDate = Carbon::parse($startPeriod)->format('d/m/Y') . ' - ' . Carbon::parse($endPeriod)->format('d/m/Y');
        $code = 'FAKE_DS_HVC_' . $periodDate.'_'.Str::slug($client->name,'-').'_'.Str::random(5);

        $crossCheckId = DB::table('cross_checks')->insertGetId([
            'path_file' => 'public/cross-checker/fake',
            'name' => $code,
            'code' => $code,
            'user_id' => $user->id,
            'client_id' => $client->id,
            'type' => $type,
            'status' => $status,
            'start_period' => $startPeriod,
            'end_period' => $endPeriod,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->info('Tao thanh cong cross_check ID '.$crossCheckId);
        $number = $this->ask('So cross_check_item can tao ', 10);
        $bill = collect();
        $this->info('1: Khop, 2: Khong ton tai trong ky doi soat, 3: Khong ton tai trong file doi soat, 4: Van don chua hoan thanh, 5: Van don da doi soat, 6: Lech tien');
        $statusCrossCheckItem = $this->ask('Trang thai cua cross_check_item', rand(1,8));
        for ($i = 1; $i <= $number; $i++) {
            $billLading = DB::table('bill_lading')->inRandomOrder()->first();
            $array = [
                'bill_lading_code' => $billLading->code,
                'xls_cod' => rand(0, 500),
                'xls_fee' => rand(0, 300),
                'status' => $statusCrossCheckItem,
                'cross_check_id' => $crossCheckId,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'sys_cod' => rand(0,500),
                'sys_fee' => rand(0,300),
                'xls_completed_date' => Carbon::now()->format('Y-m-d'),
                'sys_completed_date' => Carbon::now()->format('Y-m-d'),
                'xls_cps' => rand(0,20),
                'sys_cps' => rand(0,40)
            ];
            $bill->push($array);
            $this->info('Fake thanh cong cross check item ' . $code);
            if ($bill->count() == 1000) {
                $this->info('Insert ' . $bill->count() . ' cross check item ...');
                DB::table('cross_check_items')->insert($bill->toArray());
                $this->info('Insert success');
                $bill = collect();
            }
        }
        $this->info('Insert ' . $bill->count() . ' cross check item ...');
        DB::table('cross_check_items')->insert($bill->toArray());
        $this->info('Insert success');
        $this->info('Done !');
    }
}
