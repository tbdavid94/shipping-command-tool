<?php

namespace App\Console\Commands;

use App\BillLading;
use App\Shop;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateCrossCheckShop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:cross-check-shop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake cross check shop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('0 Doi soat thuong, 1 Doi soat boi hoan, den bu');
        $type = $this->ask('Loai doi soat ', 0);
        $this->info('Ky doi soat nhap dinh dang Y-m-d');
        $startPeriod = $this->ask('Ky doi soat bat dau', Carbon::now()->firstOfMonth()->format('Y-m-d'));
        $endPeriod = $this->ask('Ky doi soat ket thuc', Carbon::now()->lastOfMonth()->format('Y-m-d'));

        $periodDate = Carbon::parse($startPeriod)->format('d/m/Y') . ' - ' . Carbon::parse($endPeriod)->format('d/m/Y');
        $code = 'FAKE_DS_GH_' . $periodDate;
        $typeCrossCheck = 3;
        if ($type == 1) {
            $code = 'FAKE_DS_BH_GH_' . $periodDate;
            $typeCrossCheck = 5;
        }
        $payload = [
            'name' => $code,
            'code' => $code,
            'path_file' => '',
            'client_id' => 0,
            'type' => $typeCrossCheck,
            'start_period' => $startPeriod,
            'end_period' => $endPeriod,
            'user_id' => DB::table('users')->inRandomOrder()->first()->id,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];
        $crossCheckId = DB::table('cross_checks')->insertGetId($payload);
        $this->info('Tao thanh cong doi soat ' . $type . ' ' . $code);
        $number = $this->ask('So item can tao', 5);
        $item = collect();
        $crossCheck = DB::table('cross_checks')->find($crossCheckId);
        $shopCode = $this->ask('Shop code');

        for ($i = 1; $i <= $number; $i++) {
            //lay danh sach shop co 50K gian hang
            $shop = DB::table('shops')->inRandomOrder()->first(); //Shop::query()->has('billLading')->get()->random()->first();
//            $shop = Shop::query()->withCount('billLading')->having('bill_lading_count', '>', 20000)->inRandomOrder()->first();
            if (!empty($shopCode)){
                $shop = Shop::query()->where('code',$shopCode)->first();
            }
            $code = $crossCheck->code . '_' . $shop->code;
            $array = [
                'name' => $code,
                'code' => $code,
                'client_id' => 0,
                'type' => $typeCrossCheck,
                'status' => 0,
                'note' => '',
                'user_id' => $crossCheck->user_id,
                'start_period' => $crossCheck->start_period,
                'end_period' => $crossCheck->end_period,
                'parent' => $crossCheck->id,
                'shop_id' => $shop->id,
                'total_cps' => rand(1, 100),
                'total_fee' => rand(1, 100),
                'begin_period_wallet' => 0,
                'begin_period_balance' => 0,
                'total_balance' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ];
            $item->push($array);
            $this->info('Fake thanh cong doi soat con ' . $array['code']);
//            if ($item->count() == 1000) {
//                $this->info('Insert ' . $item->count() . ' cross_check ...');
//                DB::table('cross_checks')->insert($item->toArray());
//                $this->info('Insert success');
//                $item = collect();
//            }
        }

        $this->info('Insert ' . $item->count() . ' cross_check ...');
//        DB::table('cross_checks')->insert($item->toArray());

        $ids = [];
        foreach ($item->toArray() as $data) {
            $ids[] = DB::table('cross_checks')->insertGetId($data);
        }
        $this->info(implode(",", $ids));
        $limitBill = $this->ask('Moi gian hang bao nhieu don',5);
        $this->createCrossCheckItem($ids, $number, $crossCheckId, $limitBill);
        $this->info('Insert success');
        $this->info('Doi soat ID ' . $crossCheckId);
    }

    private function createCrossCheckItem($crossCheckIds, $number, $parentCrossCheckId, $limitBill)
    {
        $listBillCode = BillLading::query()->limit(100)->pluck('code')->toArray();
        $random_keys=array_rand($listBillCode,3);
        $limit = 100000 / $number;
        $crossCheckItems = collect();
        foreach ($crossCheckIds as $crossCheckId) {
            for ($i = 0; $i <= $limitBill; $i++) {
                $item['bill_lading_code'] = $listBillCode[$random_keys[0]];
                $item['cross_check_id'] = $crossCheckId;
                $item['xls_completed_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $item['sys_completed_date'] = Carbon::now()->format('Y-m-d H:i:s');
                $item['xls_cod'] = rand(0, 13);
                $item['sys_cod'] = rand(0, 13);
                $item['sys_created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                $item['sys_client_code'] = Str::random(4);
                $item['sys_client_name'] = Str::random(4);
                $item['sys_service_code'] = Str::random(4);
                $item['sys_service_name'] = Str::random(5);
                $item['sys_kv_status'] = rand(0, 13);
                $item['sys_invoice'] = Str::random(5);
                $item['parent_cross_check_id'] = $parentCrossCheckId;
                $crossCheckItems->push($item);
//                if ($crossCheckItems->count() == 1000) {
//                    $this->info('Insert ' . $crossCheckItems->count() . ' cross check items ...');
//                    DB::table('cross_check_items')->insert($crossCheckItems->toArray());
//                    $this->info('Insert success');
//                    $crossCheckItems = collect();
//                }
            }
        }

        $this->info('Insert ' . $crossCheckItems->count() . ' cross check items ...');
        foreach ($crossCheckItems->chunk(1000) as $crossCheckItemChunks){
            DB::table('cross_check_items')->insert($crossCheckItemChunks->toArray());
        }

        $this->info('Insert success');
        $this->info('Done !');

//        DB::table('cross_checks')->whereIn('id', $crossCheckIds)->orderByDesc('created_at')->chunk(1000, function ($crossChecks) use ($limit) {
//            foreach ($crossChecks as $crossCheck) {
//                BillLading::query()->with(['client', 'services'])->where('shop_id', $crossCheck->shop_id)->orderByDesc('created_at')->limit($limit)->chunk(1000, function ($billLadingOfShops) use ($crossCheck) {
//                    foreach ($billLadingOfShops as $billLadingOfShop) {
//                        $item['bill_lading_code'] = $billLadingOfShop->code;
//                        $item['cross_check_id'] = $crossCheck->id;
//                        $item['xls_completed_date'] = $billLadingOfShop->completed_date;
//                        $item['sys_completed_date'] = $billLadingOfShop->completed_date;
//                        $item['xls_cod'] = $billLadingOfShop->cod;
//                        $item['sys_cod'] = $billLadingOfShop->cod;
//                        $item['sys_created_at'] = $billLadingOfShop->created_at;
//                        $item['sys_client_code'] = $billLadingOfShop->client->code;
//                        $item['sys_client_name'] = $billLadingOfShop->client->name;
//                        $item['sys_service_code'] = $billLadingOfShop->services->code ?? '';
//                        $item['sys_service_name'] = $billLadingOfShop->services->name ?? '';
//                        $item['sys_kv_status'] = $billLadingOfShop->kv_status;
//                        $item['sys_invoice'] = $billLadingOfShop->invoice;
//
//                        DB::table('cross_check_items')->insert($item);
//                    }
//                });
//            }
//        });
    }
}
