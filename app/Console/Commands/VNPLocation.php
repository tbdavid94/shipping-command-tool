<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use Mockery\Exception;

class VNPLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vnp:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'VNP location';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = DB::table('Mapping_Location_VNP')->get();
        DB::beginTransaction();
        try {
            foreach ($data as $item){
                DB::table('cities')->where('id',$item->city_id)->update(['vnp_id' => $item->city_vnp_id]);
                DB::table('districts')->where('id',$item->district_id)->update(['vnp_id' => $item->district_vnp_id]);
                DB::table('wards')->where('id',$item->ward_id)->update(['vnp_id' => $item->ward_vnp_id]);
            }
            DB::commit();
        }catch (Exception $exception){
            $this->error($exception->getMessage());
            DB::rollBack();
        }

    }
}
