<?php

namespace App\Console\Commands;

use App\Shop;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateBillLadingBeforeCrossCheckShop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:bill-cross-check-shop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create bill before create cross check shop test performance 50K';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $skipShopKV = ['beef4'];

        DB::table('shops')->whereNotIn('code',$skipShopKV)->orderByDesc('created_at')->chunk(1000, function ($shops){
           foreach ($shops as $shop){
               $shopId = $shop->id;
               $billLadings = DB::table('bill_lading')->where('shop_id',$shopId)->count();
               if ($billLadings < 50000){
                   $this->info('Shop '.$shop->code.' < 50k van don');
                   $bill = collect();
                   for ($i = 1; $i <= 50000; $i++) {
                       $code = Str::random(8);
                       $array = [
                           'client_id' => 1,
                           'shop_id' => $shopId,
                           'kv_status' => rand(0, 13),
                           'code' => $code,
                           'invoice' => 'HD' . Str::random(4),
                           'payer' => "NGUOIGUI",
                           'completed_date' => Carbon::now()->format('Y-m-d H:i:s'),
                           'cod' => 0,
                           'fee' => rand(10, 100),
                           'service_id' => 1,
                           'from_mobile' => '',
                           'from_address' => '',
                           'to_fullname' => '',
                           'to_mobile' => '',
                           'to_address' => '',
                           'total' => 5,
                           'date' => date('d/m/Y H:i:s', strtotime('+1day')),
                           'from_ward_id' => 13,
                           'from_district_id' => 1,
                           'from_province_id' => 1,
                           'to_ward_id' => 682,
                           'to_district_id' => 35,
                           'to_province_id' => 2,
                           'service' => 1,
                           'status' => 100,
                           'note_code' => 'FAKE_CROSS_CHECK_SHOP',
                           'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                           'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                       ];
                       $bill->push($array);
                       $this->info('Fake thanh cong van don ' . $code);
                       if ($bill->count() == 1000) {
                           $this->info('Insert ' . $bill->count() . ' bill lading ...');
                           DB::table('bill_lading')->insert($bill->toArray());
                           $this->info('Insert success');
                           $bill = collect();
                       }
                   }
               }
           }
        });
    }
}
