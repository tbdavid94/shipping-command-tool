<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class StandardizedBillLading extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'standardized:bill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Standardized bill lading fix datetime error format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $count =  DB::table('bill_lading')->where('completed_date','0000-00-00 00:00:00')->count();
        $this->info(' Co '.$count.' van don bi loi dinh dang datetime completed_date');
        if ($count > 0){
            DB::table('bill_lading')->where('completed_date','=','0000-00-00 00:00:00')
                ->update(['completed_date' => now()->format('Y-m-d H:i:s')]);
        }
    }
}
