<?php

namespace App\Console\Commands;

use App\Shop;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateBillLading extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:bill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake bill lading';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '256M');
//        $shopRandom = DB::table('shops')->where('type',1)->inRandomOrder()->first();
        $clientRandom = DB::table('clients')->inRandomOrder()->first();
        $number = $this->ask('So van don can tao ', 1);
        $shopNumber = $this->ask('So gian hang ', 1);
        $shopType = $this->ask('Loai gian hang (3: Fake)',3);
        $shops = Shop::query()->select('id','code','type')->where('type',$shopType)->take($shopNumber)->inRandomOrder()->get();
        $noteCode = $this->ask('Ghi chu ', 'fake');
        $this->info('Gian hang random ' . implode(", ",$shops->pluck('code')->toArray()));
//        $shopId = $this->ask('ID Gian hang ', $shopRandom->id);
        $this->info('HVC random ' . $clientRandom->name);
        $client = $this->ask('ID HVC ', $clientRandom->id);
        $day = '2019-08-' . rand(1, 30);
        $completedDate = \Carbon\Carbon::parse($day)->format('Y-m-d');
        $service = DB::table('services')->where('client_id', $client)->inRandomOrder()->first()->code;
        $serviceId = DB::table('services')->where('client_id', $client)->inRandomOrder()->first()->id;
        $kvStatus = $this->ask('Trang thai van don',rand(0,13));
        $completedDateAsk = $this->ask('Ngay hoan thanh',$completedDate);
        $crossCheckCod = $this->ask('Doi soat COD',0);
        $crossCheckShop = $this->ask('Doi soat Shop',0);
        $parent = $this->ask('parent',0);
        $crossCheckFee = $this->ask('Doi soat Fee',0);
        $payer = $this->ask('Payer','NGUOIGUI');
        $billSpecial = $this->ask('Insert vao bang bill_lading_{special}','');
        if (!empty($billSpecial) && !Schema::hasTable('bill_lading_'.$billSpecial)){
            $this->error('Khong ton tai bang bill_lading_'.$billSpecial);
        }
        $bill = collect();
        foreach ($shops as $shop){
            for ($i = 1; $i <= $number; $i++) {
                $code = Str::random(8);
                $array = [
                    'client_id' => $client,
                    'shop_id' => $shop->id,
                    'kv_status' => $kvStatus,
                    'code' => $code,
                    'invoice' => 'HD' . Str::random(4),
                    'payer' => $payer,
                    'completed_date' => $completedDateAsk,
                    'cod' => rand(1000,50000),
                    'fee' => rand(10, 100),
                    'service_id' => $serviceId,
                    'from_mobile' => '',
                    'from_address' => '',
                    'to_fullname' => '',
                    'to_mobile' => '',
                    'to_address' => '',
                    'total' => 5,
                    'date' => date('d/m/Y H:i:s', strtotime('+1day')),
                    'from_ward_id' => 13,
                    'from_district_id' => 1,
                    'from_province_id' => 1,
                    'to_ward_id' => 682,
                    'to_district_id' => 35,
                    'to_province_id' => 2,
                    'service' => $service,
                    'cross_check_cod_id' => $crossCheckCod,
                    'cross_check_shop_id' => $crossCheckShop,
                    'cross_check_fee_id' => $crossCheckFee,
                    'status' => 100,
                    'parent_id' => $parent,
                    'note_code' => $noteCode,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ];
                $bill->push($array);
                $this->info('Fake thanh cong van don ' . $code);
                if ($bill->count() == 1000) {
                    $this->info('Insert ' . $bill->count() . ' bill lading ...');
                    if (!empty($billSpecial)){
                        DB::table('bill_lading_'.$billSpecial)->insert($bill->toArray());
                    }else{
                        DB::table('bill_lading')->insert($bill->toArray());
                    }
                    $this->info('Insert success');
                    $bill = collect();
                }
            }
        }

        $this->info('Insert ' . $bill->count() . ' bill lading ...');
        if (!empty($billSpecial)){
            DB::table('bill_lading_'.$billSpecial)->insert($bill->toArray());
        }else{
            DB::table('bill_lading')->insert($bill->toArray());
        }
        $this->info('Insert success');
        $this->info('Done !');
    }
}
