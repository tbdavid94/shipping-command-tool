<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MappingLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map:jt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $wardShips = DB::table('ward_detail')->get();
//        foreach ($wardShips as $wardShip){
//            $wardShipId = $wardShip->ward_id;
//            $jt = DB::table('ward_j&t')->where('ward_name',$wardShip->name)
//                ->where('district_name',$wardShip->district_name)
//                ->where('city_name',$wardShip->city_name)
//                ->where('status','!=',1);
//
//            $f = $jt->first();
//            if ($f){
//                $wardCodeJt = $f->ward_code;
//                $jt->update(['status' => 1]);
//                DB::table('wards')->where('id',$wardShipId)->update(['j&t_id'=>$wardCodeJt]);
//                $this->info($wardCodeJt);
//            }else{
//                $this->warn('K thay');
//            }
//        }
        $wardShips = DB::table('ward_detail')->whereNull('j&t_id')->get();
        foreach ($wardShips as $wardShip){
            $this->info($wardShip->name.' - '.$wardShip->district_name.' - '.$wardShip->city_name);
            $jt = DB::table('ward_j&t')->where('ward_name',trim($wardShip->name))
                ->where('district_name',trim($wardShip->district_name))
                ->where('city_name',trim($wardShip->city_name));

            $f = $jt->first();
            if ($f){
                $wardCodeJt = $f->ward_code;
                $jt->update(['status' => 1]);
                DB::table('wards')->where('id',$wardShip->ward_id)->update(['j&t_id'=>$wardCodeJt]);
                $this->info($wardCodeJt);
            }else{
                $this->warn('K thay');
            }
        }
    }
}
