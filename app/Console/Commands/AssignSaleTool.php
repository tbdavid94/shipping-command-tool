<?php

namespace App\Console\Commands;

use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Common\Type;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use Box\Spout\Reader\ReaderFactory;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AssignSaleTool extends Command
{
    protected $signature = 'assign:sale {day?}';

    protected $description = 'Assign sale operation';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $day = $this->argument('day');
        $startDate = isset($day) ? $day : Carbon::now()->firstOfMonth()->toDateString();

        $this->info($startDate);

        $client = new \Google_Client();
        $client->setApplicationName('My PHP App');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
        $client->setAccessType('offline');

        $jsonAuth = storage_path('auth.json');
        $jsonAuth = json_decode(file_get_contents($jsonAuth), true);
        $client->setAuthConfig($jsonAuth);

        $service = new \Google_Service_Sheets($client);
        $spreadsheetId = env('GG_SHEET_ID');
        $range = 'A2:B';
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();

        $shopUsers = collect();
        $itemFails = collect();

        foreach ($values as $value){
            $shopCode = $value[0];
            $userName = $value[1];

            $shop = DB::table('shops')->where('code', $shopCode)->first();
            $user = DB::table('users')->where('name', $userName)->orWhere('email', $userName)->first();

            if ($shop && $user) {
                $this->info($user->name);
                $shopUser['shop_id'] = $shop->id;
                $shopUser['user_id'] = $user->id;
                $shopUser['status'] = 1;
                $shopUser['start_date'] = $startDate;
                $shopUser['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                $shopUser['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

                $shopUsers->push($shopUser);//tao mot mang de insert bulk
            } else {
                $this->warn('Khong tim thay gian hang ' . $shopCode . ' va nhan vien ' . $userName . ' tren shipping');
                $itemFails->push([
                    'shop_code' => $shopCode,
                    'user_name' => $userName
                ]);
            }
        }

        if ($shopUsers->count()) {
            if (app()->environment('local')) {
                DB::table('shop_users')->truncate();
            }
            DB::table('shop_users')->insert($shopUsers->toArray());
        }else{
            if (app()->environment('local')) {
                DB::table('shop_users')->truncate();
            }
        }

        if ($itemFails->count()) {
            if (app()->environment('local')) {
                DB::table('fails')->truncate();
                DB::table('fails')->insert($itemFails->toArray());
            }
            //TODO log
        }else{
            if (app()->environment('local')) {
                DB::table('fails')->truncate();
            }
        }

        $this->info('Success !');
    }

    private function updateRowSheet($values, $service)
    {
        // Build the RowData
        $rowData = new \Google_Service_Sheets_RowData();
        $rowData->setValues($values);
        // Prepare the request
        $append_request = new \Google_Service_Sheets_AppendCellsRequest();
        $append_request->setSheetId(0);
        $append_request->setRows($rowData);
        $append_request->setFields('userEnteredValue');
        // Set the request
        $request = new \Google_Service_Sheets_Request();
        $request->setAppendCells($append_request);
        // Add the request to the requests array
        $requests = array();
        $requests[] = $request;
        // Prepare the update
        $batchUpdateRequest = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
            'requests' => $requests
        ));
        $spreadsheetId = env('GG_SHEET_ID');
        try {
            // Execute the request
            $response = $service->spreadsheets->batchUpdate($spreadsheetId, $batchUpdateRequest);
            if( $response->valid() ) {
                // Success, the row has been added
                $this->info("Them moi thanh cong log");
            }
        } catch (Exception $e) {
            // Something went wrong
            $this->error($e->getMessage());
        }
    }

    public function getFileAndRead()
    {
        $filePath = public_path('AssignSale.xlsx');
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->setShouldFormatDates(true);
        $reader->open($filePath);

        $shopUsers = collect();
        $itemFails = collect();

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $key => $row) {
                $shopCode = $row[1];
                $userName = $row[2];

                $shop = DB::table('shops')->where('code', $shopCode)->first();
                $user = DB::table('users')->where('name', $userName)->orWhere('email', $userName)->first();

                if ($shop && $user) {
                    $this->info($user->name);
                    $shopUser['shop_id'] = $shop->id;
                    $shopUser['user_id'] = $user->id;
                    $shopUser['status'] = 1;
                    $shopUser['start_date'] = Carbon::now()->format('Y-m-d');//ngay bat dau cham soc cua nhan vien se la ngay chay tool nay
                    $shopUser['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
                    $shopUser['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

                    $shopUsers->push($shopUser);//tao mot mang de insert bulk
                } else {
                    $this->warn('Khong tim thay gian hang ' . $shopCode . ' va nhan vien ' . $userName . ' tren shipping');
                    $itemFails->push([
                        'shop_code' => $shopCode,
                        'user_name' => $userName
                    ]);
                }
            }
        }

        if ($shopUsers->count()) {
            if (app()->environment('local')) {
                DB::table('shop_users')->truncate();
            }
            DB::table('shop_users')->insert($shopUsers->toArray());
        }

        if ($itemFails->count()) {
            if (app()->environment('local')) {
                DB::table('fails')->truncate();
                DB::table('fails')->insert($itemFails->toArray());
            }
        }

        $this->info('Success !');
    }
}
