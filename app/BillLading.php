<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillLading extends Model
{
    protected $table = 'bill_lading';

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function services()
    {
        return $this->belongsTo(Service::class);
    }
}
