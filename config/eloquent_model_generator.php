<?php

return [
    'model_defaults' => [
        'namespace'       => 'App\\Models',
        'base_class_name' => \Illuminate\Database\Eloquent\Model::class,
        'output_path'     => env('MODEL_PATH','/var/www/kiotviet-shipping/app/Models/Db'),
        'no_timestamps'   => null,
        'date_format'     => null,
        'connection'      => null,
        'backup'          => null,
    ],
];
