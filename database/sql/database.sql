-- convert Laravel migrations to raw SQL scripts --

-- migration:2014_10_12_000000_create_users_table --
create table `users` (
  `id` bigint unsigned not null auto_increment primary key, 
  `name` varchar(255) not null, 
  `email` varchar(255) not null, 
  `email_verified_at` timestamp null, 
  `password` varchar(255) not null, 
  `remember_token` varchar(100) null, 
  `created_at` timestamp null, 
  `updated_at` timestamp null
) default character set utf8 collate 'utf8_unicode_ci';
alter table 
  `users` 
add 
  unique `users_email_unique`(`email`);

-- migration:2014_10_12_100000_create_password_resets_table --
create table `password_resets` (
  `email` varchar(255) not null, 
  `token` varchar(255) not null, 
  `created_at` timestamp null
) default character set utf8 collate 'utf8_unicode_ci';
alter table 
  `password_resets` 
add 
  index `password_resets_email_index`(`email`);

-- migration:2019_08_19_000000_create_failed_jobs_table --
create table `failed_jobs` (
  `id` bigint unsigned not null auto_increment primary key, 
  `connection` text not null, `queue` text not null, 
  `payload` longtext not null, `exception` longtext not null, 
  `failed_at` timestamp default CURRENT_TIMESTAMP not null
) default character set utf8 collate 'utf8_unicode_ci';

-- migration:2020_07_22_143632_create_table_shop_levels --
create table `shop_levels` (
  `id` int unsigned not null auto_increment primary key, 
  `code` varchar(10) not null default '1', 
  `name` varchar(255) null, 
  `status` tinyint not null default '1', 
  `created_at` timestamp null, 
  `updated_at` timestamp null, 
  `deleted_at` timestamp null
) default character set utf8 collate 'utf8_unicode_ci';

-- migration:2020_07_22_145021_add_shop_level --
alter table 
  `shops` 
add 
  `shop_level` varchar(10) not null default '1' 
after 
  `code`;
